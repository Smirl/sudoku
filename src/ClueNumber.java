import java.io.*;

/**
 * Find the phase transition data for non unique puzzles 
 * @author s0941897
 *
 */
public class ClueNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException {

		double minTemp =0;
		double maxTemp=2;
		int points = 100;
		int runs = 100;
		int bootTimes = 100;
		int pastEnergyLength = 150000;
		String fileName = args[0];
		int clueMin = Integer.parseInt(args[1]);
		int clueMax = Integer.parseInt(args[2]);

		if(args.length<3) System.out.println("fileName clueMin clueMax");

		PrintWriter out = new PrintWriter(new File(fileName));

		for(int i=clueMin;i<clueMax;i++){
		
			for(int t=0;t<points;t++){
				
				double temp =((maxTemp-minTemp)/points)*(double)t + minTemp;

				double[] tempEnergy = new double[runs];
				double[] tempHeat = new double[runs];

				for(int r=0;r<runs;r++){
					
					int c =0;
					double[] pastEnergy = new double[pastEnergyLength];
					int pastEnergyIndex =0;
					boolean small = true;

					SudokuSolver ss = new SudokuSolver(9, 0.0);
					ss.fillGridWithRules();
					
					//create gird
					while(!ss.isStopped() && small){
						//restart conditions
						pastEnergyIndex = c%pastEnergyLength;
						pastEnergy[pastEnergyIndex] = ss.getEnergy();
						if(pastEnergyIndex==0){
							if(SudokuSolver.mean(pastEnergy)==pastEnergy[0] ){
								small=false;
								r--;
							}
						}
						ss.iterate();
						c++;
					}
									
					//
					double[] runEnergy = new double[100];

					//if sovled a grid then do SCIENCE
					if(small){
					    
    						for(int k=0;k<81-i;k++){
							int x = (int)(Math.random()*9.0);
							int y = (int)(Math.random()*9.0);
							
							if(ss.getGridElement(x,y)!=0){
							    ss.setGridElement(x, y, 0);
							}else{
							    k--;
							}
						}
						
						ss.setClues(ss.getCluesForMain());
						ss.fillGridWithRules();

						ss.setT(temp);

						//get o equil
						ss.iterate(100000);

						for(int j=0;j<runEnergy.length;j++){
						    
							//read off values
						    runEnergy[j] = ss.getEnergy();
						    ss.iterate(500);					    

						}
					}		
					tempEnergy[r] = SudokuSolver.mean(runEnergy);
					tempHeat[r] = SudokuSolver.stDev(runEnergy);
					System.out.println(i+" "+t+" "+r);
				}
					//bootstrap for heat capacity
				double[] boot = SudokuSolver.bootstrap(tempHeat, bootTimes);
				
				out.println(i+" "+temp+" "+SudokuSolver.mean(tempEnergy)+" "+SudokuSolver.stDev(tempEnergy)+" "+boot[0]*(1/(temp*temp))+" "+boot[1]);
				out.flush();
				
			}
			out.printf("%n%n");
		}
		out.close();
	}    
}