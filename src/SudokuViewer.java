import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

/**
 * 
 */

/**
 * Draws the sudoku grid on the panel with text boxes.
 * @author s0941897
 *
 */
public class SudokuViewer extends JPanel {

	private Sudoku controller;

	private JTextField[][] elements;
	public Color gray = new Color(200,200,200);

	private Timer autoUpdater = new Timer(50, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) { 
			synchronized (this) {
				updateAll();
			}
		}		
	});

	/**
	 * 
	 */
	public SudokuViewer(Sudoku attachedTo) {
		controller = attachedTo;
		setOpaque(true);
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(600,600));

		int[][] grid = controller.getCurrentState();


		elements = new JTextField[grid.length][grid.length];

		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				elements[j][i] = new JTextField();
			}
		}

		double w = getPreferredSize().getWidth();
		double spacing = (double)grid.length*5 + 5;
		int width = (int)((w-spacing)/(double)grid.length);

		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				elements[j][i].setPreferredSize(new Dimension(width,width));
				int val = grid[j][i];
				if(val!=0) elements[j][i].setText(Integer.toString(val));
				else elements[j][i].setText("");
				elements[j][i].setHorizontalAlignment(JTextField.CENTER);
				elements[j][i].setAlignmentY(JTextField.CENTER);
				elements[j][i].setFont(new Font("Arial",Font.PLAIN,22));

				add(elements[j][i]);
				elements[j][i].setBounds(j*width, i*width, width, width);
			}
		}



	}

	public Point[] getClues(){
		//get number of clues
		int c =0;
		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				if(!elements[j][i].getText().equals("")) c++;
			}
		}
		Point[] p = new Point[c];
		c=0;
		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				if(!elements[j][i].getText().equals("")) {
					p[c] = new Point(j,i);
					elements[j][i].setBackground(gray);
					c++;
				}
			}
		}
		return p;
	}
	public int[][] getGrid(){
		int[][] g = new int[controller.getN()][controller.getN()];
		for (int i = 0; i < elements.length; i++) {
			for (int j = 0; j < elements.length; j++) {
				if(!elements[j][i].getText().equals("")) g[j][i] = Integer.parseInt(elements[j][i].getText());
				else g[j][i] = 0;
			}
		}
		return g;
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);

		g.setColor(Color.black);
		g.fillRect(197, 0, 7, 600);
		g.fillRect(395, 0, 7, 600);
		g.fillRect(0, 197, 600, 7);
		g.fillRect(0, 395, 600, 7);
	}

	public void simulationStarted(){
		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				elements[j][i].setEnabled(false);
			}
		}
		autoUpdater.restart();
	}

	public void simulationStoped(){
		autoUpdater.stop();
		updateAll();
		repaint();
		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				elements[j][i].setEnabled(true);
			}
		}
	}
	public void updateAll(){
		int[][] grid = controller.getCurrentState();
		for(int i=0;i<elements.length;i++){
			for(int j=0;j<elements.length;j++){
				if(grid[j][i]==0) {
					elements[j][i].setText("");
					elements[j][i].setBackground(Color.white);
				}
				else elements[j][i].setText(Integer.toString(grid[j][i]));
			}
		}
	}

}
