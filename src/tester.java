import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 
 */

/**
 * @author s0941897
 *
 */
public class tester {

	public static void main(String[] args) throws IOException {
		for (int d = 0; d < 5; d++) {



			PrintWriter out = new PrintWriter(new File(System.getProperty("user.dir")+"/data/diff"+d+".dat"));

			double maxTemp = 2;
			double minTemp = 0;
			int points = 20;
			int runs = 5;
			int bootTimes = 100;


			for (int i = 0; i < points; i++) {
				double temp = ((maxTemp-minTemp)/points)*(double)i + minTemp;

				double[] tempEnergy = new double[runs];
				double[] tempHeat = new double[runs];

				for(int r=0; r<runs; r++){
					SudokuSolver ss = new SudokuSolver(9, temp);
					ss.setClues(ss.getCluesForMain());
					ss.fillGridWithRules();

					ss.iterate(100000);

					double[] runEnergy = new double[100];

					for (int j = 0; j < runEnergy.length; j++) {
						runEnergy[j] = ss.getEnergy();
						ss.iterate(500);
					}
					
					tempEnergy[r] = SudokuSolver.mean(runEnergy);
					tempHeat[r]   = SudokuSolver.stDev(runEnergy);
					
					System.out.println(d+" "+i+" "+r);

				}



				double[] boot = SudokuSolver.bootstrap(tempHeat, bootTimes);

				out.println(temp+" "+SudokuSolver.mean(tempEnergy)+" "+SudokuSolver.stDev(tempEnergy)
						+" "+boot[0]*(1/(temp*temp))+" "+boot[1]);
				out.flush();


			}
			out.close();
		}
	}





	/*public static void main(String[] args) throws IOException {



		double maxTemp = 1;
		double minTemp = 0;
		int points = 5;


		for (int i = 0; i < points+1; i++) {
			double temp = ((maxTemp-minTemp)/points)*(double)i + minTemp;
			PrintWriter out = new PrintWriter(new File(System.getProperty("user.dir")+"/data/"+temp));

			SudokuSolver ss = new SudokuSolver(9, temp);
			ss.setClues(ss.getCluesForMain());
			ss.fillGridWithRules();

			int t =0;

			for(int j=0; j<100000;j++) {
				ss.iterate();
				out.println(t+" "+ss.getEnergy());
				out.flush();
				t++;
			}

			out.close();

		}

	}*/
}
