import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.RestoreAction;

/**
 * The user panel for the GUI system. Adds compenents and styles them
 * @author s0941897
 *
 */
public class SudokuUserPanel extends JPanel {

	private Sudoku controller;

	private JButton startStop = new JButton("START");
	private JButton reset = new JButton("RESET");
	private JButton sample = new JButton("NEW");
	private JButton clues = new JButton("SET CLUES");
	private JSlider tempSlider = new JSlider(JSlider.VERTICAL,0, 100, 40);
	private JLabel tempLabel = new JLabel("Temp: 0.4");
	private JCheckBox correct = new JCheckBox("", false);
	private JTextField diff = new JTextField("6");
	
	private boolean CORRECT = false;

	public static Color redRidingHood = new Color(209,72,54);
	public Color darkGray = new Color(50, 50, 50);
	public Color lightGray = new Color(250,250,250);
	public Color gray = new Color(200,200,200);

	public SudokuUserPanel(Sudoku attachedTo){
		controller=attachedTo;
		setOpaque(true);
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(100,600));

		startStop.setBackground(redRidingHood);
		startStop.setForeground(Color.WHITE);
		startStop.setPreferredSize(new Dimension(80, 35));
		startStop.setFocusable(false);

		reset.setBackground(darkGray);
		reset.setForeground(Color.WHITE);
		reset.setPreferredSize(new Dimension(80, 35));
		reset.setFocusable(false);
		
		diff.setPreferredSize(new Dimension(37,35));
		
		sample.setBackground(darkGray);
		sample.setForeground(Color.WHITE);
		sample.setPreferredSize(new Dimension(37,35));
		sample.setFont(new Font("Arial", Font.BOLD, 12));
		sample.setMargin(new Insets(0, 0, 0, 0));
		sample.setFocusable(false);

		clues.setBackground(darkGray);
		clues.setForeground(Color.WHITE);
		clues.setPreferredSize(new Dimension(80, 35));
		clues.setFocusable(false);
		clues.setMargin(new Insets(0, 0, 0, 0));

		//Create the label table
		Hashtable labelTable = new Hashtable();
		labelTable.put( new Integer( 0 ), new JLabel("0") );
		labelTable.put( new Integer( 100 ), new JLabel("1") );
		
		tempSlider.setLabelTable( labelTable );
		tempSlider.setPaintLabels(true);
		tempSlider.setMajorTickSpacing(10);
		tempSlider.setPaintTicks(true);
		tempSlider.setPreferredSize(new Dimension(80, 300));
		tempSlider.setBackground(Color.WHITE);
		tempSlider.setForeground(redRidingHood);
		
		correct.setBackground(lightGray);
		correct.setPreferredSize(new Dimension(35, 35));
		correct.setFocusable(false);
		correct.setEnabled(false);
		
		startStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.running() == true){
					controller.stopSimulation();
				}else{
					controller.startSimulation();
				}

			}
		});

		reset.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.reset();

			}
		});
		
		sample.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.sample(Integer.parseInt(diff.getText()));
			}
		});

		clues.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.setClues();

			}
		});

		tempSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				double val = (double)tempSlider.getValue()/100.0;
				controller.setT(val);
				tempLabel.setText("Temp:" + val);
			}
		});

		add(startStop);
		add(reset);
		add(diff);
		add(sample);
		add(clues);
		add(tempLabel);
		add(tempSlider);
		add(correct);
	}
	
	public void switchCheck(){
		correct.setSelected(true);
	}

	public void simulationStarted(){
		startStop.setText("STOP");
		reset.setEnabled(false);
		clues.setEnabled(false);
		sample.setEnabled(false);
		correct.setSelected(false);
	}
	public void simulationStopped(){
		startStop.setText("START");
		reset.setEnabled(true);
		clues.setEnabled(true);
		sample.setEnabled(true);
	}

}
