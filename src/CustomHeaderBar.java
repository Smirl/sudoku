import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class CustomHeaderBar extends JPanel {
	
	private Sudoku controller;
	
	
	private JLabel heading = new JLabel("Sudoku Solver");
	

	public CustomHeaderBar(Sudoku attachedTo) {

		controller = attachedTo;
		setOpaque(true);
		setBackground(Color.WHITE);
		setPreferredSize(new Dimension(600,50));
		
		heading.setFont(new Font("Arial", Font.PLAIN, 18));
		heading.setForeground(new Color(221,75,57));
		
		this.setLayout(null);
		
		add(heading);
	
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 4, 0, new Color(200,200,200)));
		
		heading.setBounds(10, 15, 300, 20);
		
		

	}	
}
