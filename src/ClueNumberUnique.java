import java.io.*;
import java.lang.Math.*;

/**
 * Get the phase transition data for downloaded unique puzzles. (May have to fix paths to files)
 * @author s0941897
 *
 */
public class ClueNumberUnique {

    public static void main(String[] args) throws IOException {

	if(args.length<3) {
	    System.out.println("fileName clueMin clueMax");
	    System.exit(1);
	}
	
	double minTemp =0;
	double maxTemp=2;
	int points = 100;
	int runs = 100;
	int bootTimes = 100;
	int pastEnergyLength = 150000;
	String fileName = args[0];
	int clueMin = Integer.parseInt(args[1]);
	int clueMax = Integer.parseInt(args[2]);

	PrintWriter out = new PrintWriter(new File(fileName));

	for(int i=clueMin;i<clueMax;i++){
	    
	    for(int t=0;t<points;t++){	
		
		double temp =((maxTemp-minTemp)/points)*(double)t + minTemp;
			    
		double[] tempEnergy = new double[runs];
		double[] tempHeat = new double[runs];
			    
		for(int r=0;r<runs;r++){
		    
		    int rand =(int)(Math.random()*5)+1;
		    //get the grid from file and not create
		    SudokuSolver ss = new SudokuSolver(temp,i,rand);
		    ss.setClues(ss.getCluesForMain());
		    ss.fillGridWithRules();

		    double[] runEnergy = new double[100];

		    //get to equil
		    ss.iterate(100000);
				
		    for(int j=0;j<runEnergy.length;j++){
				    
		    	//measurements
			runEnergy[j] = ss.getEnergy();
			ss.iterate(500);					    
				    
		    }
				
		    tempEnergy[r] = SudokuSolver.mean(runEnergy);
		    tempHeat[r] = SudokuSolver.stDev(runEnergy);
		    System.out.println(i+" "+t+" "+r);
		}
			   
		//bootstrap for HC
		double[] boot = SudokuSolver.bootstrap(tempHeat, bootTimes);
			    
		//print to file
		out.println(i+" "+temp+" "+SudokuSolver.mean(tempEnergy)+" "+SudokuSolver.stDev(tempEnergy)+" "+boot[0]*(1/(temp*temp))+" "+boot[1]);
		out.flush();
			    
	    }
	    out.printf("%n");
	}
	out.close();
    }
}
    