import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Point;
import java.io.IOException;

import javax.swing.JFrame;

/**
 * 
 */

/**
 * The controller in the M-V-C structure. Should be run for GUI
 * @author s0941897
 *
 */
public class Sudoku extends JFrame {


	private SudokuSolver ss;
	private int[][] theGrid;
	private int[] changed;
	private int N;
	private double T;
	private Point[] clues;

	private SudokuViewer viewer;
	private CustomHeaderBar header = new CustomHeaderBar(this);
	private SudokuUserPanel user = new SudokuUserPanel(this);

	private Thread background = null;

	/**
	 * @param title
	 * @throws HeadlessException
	 */
	public Sudoku() throws HeadlessException {
		super("Sudoku Solver");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);

		ss = new SudokuSolver(9,0.4);
		getCurrentState();
		viewer = new SudokuViewer(this);
		setClues();

		getContentPane().add(viewer, BorderLayout.CENTER);
		getContentPane().add(user, BorderLayout.WEST);
		getContentPane().add(header, BorderLayout.NORTH);
		pack();


		repaint();

		setVisible(true);
	}

	//Getters and setters
	public int getN(){
		return N;
	}
	public void setN(int n){
		N=n;
	}
	public void setT(double n){
		T=n;
		ss.setT(n);
	}
	public void setTheGrid(int[][] g) {
		this.theGrid = g;
	}

	public int[][] getTheGrid() {
		return theGrid;
	}
	public int[][] getCurrentState(){
		synchronized (this) {
			setTheGrid(ss.getGrid());
			setN(ss.getN());
			setT(ss.getT());
		}
		return getTheGrid();
	}
	public int[] getChangedElement(){
		synchronized (this) {
			return changed;
		}
	}
	public void reset(){
		ss.initGrid();
		setTheGrid(ss.getGrid());
		viewer.updateAll();
		setClues();
	}
	public void sample(int n) {
		try {
			ss.sampleGrid(n);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		setTheGrid(ss.getGrid());
		viewer.updateAll();
		setClues();
	}
	public void setClues(){
		synchronized (this) {
			Point[] p = viewer.getClues();
			ss.setClues(p);
			clues=p;
			setTheGrid(viewer.getGrid());
		}
	}

	public boolean running(){
		if(background == null){
			return false;
		}else{
			return true;
		}
	}

	public void startSimulation() {

		if(background != null) return;
		// Set up a new simulation with the desired initial condition (since this may be different from the state in which the last simulation ended)
		ss = new SudokuSolver(theGrid,clues);
		ss.fillGridWithRules();
		ss.totalEnergy();
		getCurrentState();
		viewer.updateAll();
		// Run it in a new background thread
		background = new Thread() {
			@Override
			public void run() {

				// Communicate with the view objects on the Swing thread
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						// Inform the view objects that the simulation has started

						viewer.simulationStarted();
						user.simulationStarted();
					}
				});

				// Repeatedly iterate the equations of motion until the thread is interrupted
				while(!isInterrupted()) {
					ss.iterate(1);
					if(ss.isStopped()){
						stopSimulation();
						if(ss.checkGrid()) user.switchCheck();
					}
				}
				//TODO:maybe get the state of the grid here.

				// Communicate with the view objects on the Swing thread
				javax.swing.SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						// Remove the reference to the background thread (note, we do this
						// on the Swing thread to avoid concurrency problems)
						background = null;
						// Inform the view objects that the simulation has finished
						viewer.simulationStoped();
						user.simulationStopped();

					}
				});

			}
		};
		background.start();
	}

	public void stopSimulation() {
		// Check the a background thread is actually running
		if(background == null) return;
		// Send the interrupt message to the background thread to stop it at a convenient point
		background.interrupt();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		JFrame.setDefaultLookAndFeelDecorated(true);

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new Sudoku();

			}
		});
		

	}


}
